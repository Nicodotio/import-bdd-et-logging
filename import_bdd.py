'''
    The `import_bdd` module
    =======================

    Un script qui permet de lire un fichier CSV avec les caractéristiques
    des véhicules. Il insert les données dans une BDD. Il peut aussi mettre à jour
    les données.
'''


import csv
import sqlite3
import logging
import argparse

from urllib.request import pathname2url

root_logger = logging.getLogger()
root_logger.setLevel(logging.INFO)
handler = logging.FileHandler('import_bdd.log', 'w', 'utf-8')
formatter = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
handler.setFormatter(formatter)
root_logger.addHandler(handler)


def lectureFichier(fichier, delimiter):
    '''
    Lit le fichier passé en paramètre.

    :param fichier: lien du fichier
    :type fichier: str
    :param delimiter: séparateur entre les champs
    :type delimiter: str
    :return: liste de listes contenant les caractéristiques de chaque véhicule
    :rtype: list
    '''
    liste = []
    with open(fichier) as csvfile:
        logging.info('Lecture fichier CSV')
        reader = csv.reader(csvfile, delimiter=delimiter)
        next(reader)
        for row in reader:
            liste.append(row)
    return liste


def existenceBDD(nom):
    '''
    Vérifie l'existence d'une base de données

    :param nom: nom de la base de données ('example.sqlite3')
    :type nom: str
    :return: booléen (vrai si elle existe, faux si elle n'existe pas)
    :rtype: boolean
    '''
    dburi = "file:{}?mode=ro".format(pathname2url(nom))
    try:
        connexion = sqlite3.connect(dburi, uri=True)
        connexion.close()
    except sqlite3.OperationalError:
        logging.info('La BDD n\'existe pas')
        return False
    logging.info('La BDD existe')
    return True


def creationBDD(nom):
    '''
    Crée la base de données

    :param nom: nom de la base de données
    :type nom: str
    :return: aucun
    '''
    sqlite3.connect(nom)
    connexion = sqlite3.connect(nom)
    connexion.close()
    logging.info('Création de la base de données')


def existenceTable(curseur, nomTable):
    '''
    Verifie l'existence d'une table dans une base de données

    :param curseur: curseur qui permet d'avoir accès à la base de données
    :type curseur: Cursor
    :param nom: nom de la table
    :type nom: str
    :return: booléen (vrai si elle existe, faux si elle n'existe pas)
    :rtype: boolean
    '''
    curseur.execute(
        'SELECT count(name) FROM sqlite_master WHERE type="table" AND name=?', [nomTable])
    nb_row = curseur.fetchone()
    if(nb_row[0] == 1):
        logging.info('La table existe')
        return True
    else:
        logging.info('La table n\'existe pas')
        return False


def creationTable(curseur):
    '''
    Crée la table SIV

    :param curseur: curseur qui permet d'avoir accès à la base de données
    :type curseur: Cursor
    '''
    curseur.execute('''CREATE TABLE SIV (
        "address_titulaire"	TEXT,
        "nom"	TEXT,
        "prenom"	TEXT,
        "immatriculation"	TEXT UNIQUE,
        "date_immatriculation"	TEXT,
        "vin"	TEXT UNIQUE,
        "marque"	TEXT,
        "denomination_commerciale"	TEXT,
        "couleur"	TEXT,
        "carrosserie"	TEXT,
        "categorie"	TEXT,
        "cylindree"	INTEGER,
        "energie"	INTEGER,
        "places"	INTEGER,
        "poids"	INTEGER,
        "puissance"	INTEGER,
        "type"	TEXT,
        "variante"	TEXT,
        "version"	TEXT,
        PRIMARY KEY("immatriculation")
    )''')
    logging.info('Création de la table')


def existenceLigne(curseur, immatriculation):
    '''
    Vérifie l'existence d'une ligne dans une table

    :param curseur: curseur qui permet d'avoir accès à la base de données
    :type curseur: Cursor
    :param immatriculation: immatriculation du véhicule 
    :type immatriculation: str
    :return: booléen (vrai si elle existe, faux si elle n'existe pas)
    :rtype: boolean
    '''
    curseur.execute(
        'SELECT COUNT(immatriculation) FROM SIV WHERE immatriculation=?', [immatriculation])
    nb_row = curseur.fetchone()
    if(nb_row[0] != 1):
        return False
    else:
        return True


def creationLigne(curseur, caracteristiques):
    '''
    Crée une ligne dans la table SIV avec ses caractéristiques

    :param curseur: curseur qui permet d'avoir accès à la base de données
    :type curseur: Cursor
    :param caracteristiques: une liste des caractéristiques du véhicule
    :type param: list
    '''
    curseur.execute(
        'INSERT INTO SIV VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', caracteristiques)
    logging.info('Création d\'une ligne')


def miseAJourLigne(curseur, caracteristiques):
    '''
    Met à jour une ligne dans la table SIV avec ses caractéristiques

    :param curseur: curseur qui permet d'avoir accès à la base de données
    :type curseur: Cursor
    :param caracteristiques: une liste des caractéristiques du véhicule
    :type param: list
    '''
    curseur.execute('''UPDATE SIV SET
                        address_titulaire=?,
                        nom=?,
                        prenom=?,
                        immatriculation=?,
                        date_immatriculation=?,
                        vin=?,
                        marque=?,
                        denomination_commerciale=?,
                        couleur=?,
                        carrosserie=?,
                        categorie=?,
                        cylindree=?,
                        energie=?,
                        places=?,
                        poids=?,
                        puissance=?,
                        type=?,
                        variante=?,
                        version=?
                        WHERE immatriculation=?
                        ''', caracteristiques + [caracteristiques[3]])
    logging.info('Mise à jour d\'une ligne')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        "Insérer les données du fichier dans une base de données")
    parser.add_argument("fichierCSV", help="Le fichier CSV")
    parser.add_argument("base", help="Le chemin vers la base de données")
    args = parser.parse_args()

    liste_source = lectureFichier(args.fichierCSV, ';')
    caracteristiques = []

    connexion = sqlite3.connect(args.base)
    curseur = connexion.cursor()

    if (existenceBDD(args.base) == False):
        creationBDD(args.base)

    if (existenceTable(curseur, 'SIV') == False):
        creationTable(curseur)

    for caracteristiques in liste_source:
        immatriculation = caracteristiques[3]

        if(existenceLigne(curseur, immatriculation) == False):
            creationLigne(curseur, caracteristiques)
            connexion.commit()
        else:
            miseAJourLigne(curseur, caracteristiques)
            connexion.commit()

    connexion.close()
