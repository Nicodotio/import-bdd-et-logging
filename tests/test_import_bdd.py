import unittest
import logging
import os
import sqlite3

from import_bdd import (lectureFichier,
                        existenceBDD,
                        creationBDD,
                        existenceTable,
                        creationTable,
                        existenceLigne,
                        creationLigne,
                        miseAJourLigne)


class TestImportBDDFunctions(unittest.TestCase):
    def test_lectureFichier(self):
        result = [['Nicolas', 'FONTAINE'], ['Thomas', 'NOVAREZE']]
        with self.assertLogs() as log:
            self.assertEqual(lectureFichier(
                './tests/fichier_test.csv', ','), result)
        self.assertEqual(log.output, ['INFO:root:Lecture fichier CSV'])

    def test_existenceBDD(self):
        with self.assertLogs() as log:
            self.assertEqual(existenceBDD('test.sqlite3'), False)
        self.assertEqual(log.output, ['INFO:root:La BDD n\'existe pas'])
        with self.assertLogs() as log:
            self.assertEqual(existenceBDD('tests/test2.sqlite3'), True)
        self.assertEqual(log.output, ['INFO:root:La BDD existe'])

    def test_creationBDD(self):
        self.assertEqual(existenceBDD('tests/voiture.sqlite3'), False)
        with self.assertLogs() as log:
            creationBDD('tests/voiture.sqlite3')
        self.assertEqual(
            log.output, ['INFO:root:Création de la base de données'])
        self.assertEqual(existenceBDD('tests/voiture.sqlite3'), True)
        os.remove('tests/voiture.sqlite3')

    def test_existenceTable(self):
        connexion = sqlite3.connect('tests/test2.sqlite3')
        curseur = connexion.cursor()
        self.assertEqual(existenceTable(curseur, 'marque'), False)
        curseur.execute('CREATE TABLE IF NOT EXISTS modele (id INTEGER)')
        self.assertEqual(existenceTable(curseur, 'modele'), True)
        connexion.close()

    def test_creationTable(self):
        connexion = sqlite3.connect('tests/test3.sqlite3')
        curseur = connexion.cursor()
        self.assertEqual(existenceTable(curseur, 'SIV'), False)
        with self.assertLogs() as log:
            creationTable(curseur)
        self.assertEqual(
            log.output, ['INFO:root:Création de la table'])
        self.assertEqual(existenceTable(curseur, 'SIV'), True)
        connexion.close()
        os.remove('tests/test3.sqlite3')

    def test_existenceLigne(self):
        connexion = sqlite3.connect('tests/db_with_table.sqlite3')
        curseur = connexion.cursor()
        self.assertEqual(existenceLigne(curseur, 'OVC-568'), False)
        self.assertEqual(existenceLigne(curseur, '859 GZP'), True)
        connexion.close()

    def test_creationLigne(self):
        connexion = sqlite3.connect('tests/db_with_table.sqlite3')
        curseur = connexion.cursor()
        self.assertEqual(existenceLigne(curseur, '2-2270C'), False)
        list_caracteristiques = lectureFichier('auto_modif.csv', ';')
        caracteristiques = list_caracteristiques[2]
        with self.assertLogs() as log:
            creationLigne(curseur, caracteristiques)
        self.assertEqual(
            log.output, ['INFO:root:Création d\'une ligne'])
        self.assertEqual(existenceLigne(curseur, '2-2270C'), True)
        connexion.commit()
        curseur.execute(
            'DELETE FROM SIV WHERE immatriculation="2-2270C"')
        connexion.commit()
        connexion.close()

    def test_miseAJourLigne(self):
        connexion = sqlite3.connect('tests/db_with_table.sqlite3')
        curseur = connexion.cursor()
        curseur.execute(
            'UPDATE SIV SET couleur="Blue" WHERE immatriculation="859 GZP"')
        connexion.commit()
        curseur.execute(
            'SELECT couleur FROM SIV WHERE immatriculation="859 GZP"')
        self.assertEqual(curseur.fetchone()[0], 'Blue')
        list_caracteristiques = lectureFichier('auto_modif.csv', ';')
        caracteristiques = list_caracteristiques[1]
        with self.assertLogs() as log:
            miseAJourLigne(curseur, caracteristiques)
        connexion.commit()
        curseur.execute(
            'SELECT couleur FROM SIV WHERE immatriculation="859 GZP"')
        self.assertEqual(curseur.fetchone()[0], 'Snow')
        self.assertEqual(
            log.output, ['INFO:root:Mise à jour d\'une ligne'])
        connexion.close()
